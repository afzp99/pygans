# PyGANs (Machine Learning Framework)

Python Framework for Generative Adversarial Networks. It provides an Abstraction layer to create and train GANs with an integrated monitoring module to see the evolution of the Network through time.

There are incorporated examples that show the usage of the framework in 2D functions and generating Hand Written Digits.

Tested Models:
* Trigonometric Functions (f(x)=sin(x))  
*  Polynomial Functions, for example:  
O f(x) = x\*\*2  
O f(x) = x\*\*5 - 3x\*\*2 - 4  

<img src="images/funct_2d.gif" width="400" height="300"/>

* HandWritten Digits (MNIST)

<img src="images/mnist_first_epoch.gif" width="300" height="200"/>
<img src="images/mnist-epochs.png" width="300" height="200"/>

## Installation

PyGANs is available in the Python Package Index (PyPI), so it's possible to install it using the package installer for Python (pip), which install automatically the external dependencies:  

**$**  pip install pygans

## External Dependencies

If you install the library using **pip**, it will install automatically the dependencies.

* Keras
* Tensorflow
* Numpy
* Matplotlib

## Versions

**2.0.3**  

  BugFix: Random Generation in Dataset_Generator_2D  

**2.0.2**  

* BugFix: MNIST Animation  

**2.0.0**  

* MNIST Supported  
* Simplified API  
* Optimizing training process  
* BugFix: Latent points generation on Standard normal distribution  

**1.0.0**  

* 2D Functions tested  
* 2D animation supported  

## Components

PyGANs is composed by three modules:

**models:** Contains the Discriminator, Generator and GAN classes.  
**dataset:** The responsibility of delivering a balanced batch composed by new generated data and real samples is in Dataset_Generator class.  
**animation:** There is an optional step in the GANs pipeline, which is visualize the evolution of Generator and Discriminator over time. Animation class helps in that process.  

<img src="images/uml.png" width="500" height="400"/>

## Stages

**0)** Import the needed modules

<pre>
from pygans.models import Discriminator, Generator, GAN
from pygans.animation import Animation_2D
from pygans.dataset import Dataset_Generator_2D
</pre>

**1)** Define your Discriminator and Generator model in Keras (Sequential class)

<pre>
def create_generator_model():
  model = Sequential()
  model.add(Dense(10, ...))
  ...
  return model

def create_discriminator_model():
  model = Sequential()
  model.add(...)
  ...
  model.compile(loss="binary_crossentropy", ...)
  return model
</pre>


**2)** Define your hyperparameters

<pre>
latent_dims = 5
batch_size = 256
epochs = 3
batches = dataset_size // batch_size
iterations = batches * epochs
</pre>

**3)** Create your Dataset Generator

<pre>
dataset_generator = Dataset_Generator_2D()
</pre>

If you want to use the framework in a different type of project, you can implement your own Dataset_Generator, implementing the method **generate_real_samples(n)**, which should deliver a numpy array with **n** real samples:

<pre>
from pygans.dataset import Dataset_Generator

class Dataset_Generator_Faces(Dataset_Generator):
  def generate_real_samples(self, n):
    faces = load_faces_from_somewhere(samples=n)
    return faces

data_gen = Dataset_Generator_Faces(latent_dims)
</pre>

**4)** Create the Discriminator, Generator and GAN with the models defined previously:
<pre>
disc_model = create_discriminator_model()
gen_model = create_generator_model()

dis = Discriminator(disc_model)
gen = Generator(gen_model, latent_dims)
gan = GAN(dis, gen)
</pre>
**5)** Train the GAN
<pre>
plots, predicted_labels = gan.train(dataset_generator, iterations, batch_size, freq_generation=steps)
</pre>

**5+1)** Generate animation to see the evolution of the GAN over time.

<pre>
fps = 10
anim = Animation_2D(plots, ... , predicted_labels)
</pre>

With the Animation object you can visualize the animation as a video or generate a **GIF** image:

<pre>
anim_video = anim.get_animation(fps=fps)
anim.save(anim_video, fps, "/tmp")
</pre>
## Usage

To use PyGANs in more complex projects, its necessary to create a custom implementation of Dataset_Generator, implementing the abstract method called **generate_real_samples(n)** which delivers real data in training time.  

The method can be implemented to feed the model with real-time data or from a pre-loaded dataset.
