from .animation import Animation
from .animation_2d import Animation_2D
from .animation_mnist import Animation_MNIST
