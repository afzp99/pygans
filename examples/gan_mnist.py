from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Reshape,Conv2DTranspose
from keras.layers import Dense,Dropout,Flatten
from keras.layers import Conv2D, LeakyReLU

from keras.optimizers import Adam

from pygans.models import GAN, Discriminator, Generator
from pygans.dataset import Dataset_Generator_MNIST
from pygans.animation import Animation_MNIST

import matplotlib.pyplot as plt

def create_discriminator_model(input_shape=(28,28,1)):
	model = Sequential()
	model.add(Conv2D(64, (3,3), strides=(2, 2), padding='same', input_shape=input_shape))
	model.add(LeakyReLU(alpha=0.2))
	model.add(Dropout(0.4))
	model.add(Conv2D(64, (3,3), strides=(2, 2), padding='same'))
	model.add(LeakyReLU(alpha=0.2))
	model.add(Dropout(0.4))
	model.add(Flatten())
	model.add(Dense(1, activation='sigmoid'))
	opt = Adam(lr=0.0002, beta_1=0.5)
	model.compile(loss='binary_crossentropy', optimizer=opt , metrics=['accuracy'])
	return model


def create_generator_model(latent_dim):
	model = Sequential()
	n_nodes = 128 * 7 * 7
	model.add(Dense(n_nodes, input_dim=latent_dim))
	model.add(LeakyReLU(alpha=0.2))
	model.add(Reshape((7, 7, 128)))
	# upsample to 14x14
	model.add(Conv2DTranspose(128, (4,4), strides=(2,2), padding='same'))
	model.add(LeakyReLU(alpha=0.2))
	# upsample to 28x28
	model.add(Conv2DTranspose(128, (4,4), strides=(2,2), padding='same'))
	model.add(LeakyReLU(alpha=0.2))
	model.add(Conv2D(1, (7,7), activation='sigmoid', padding='same'))
	return model

latent_dims = 100

disc_model = create_discriminator_model()
gen_model = create_generator_model(latent_dims)
data_gen = Dataset_Generator_MNIST()
disc = Discriminator(disc_model)
gen = Generator(gen_model, latent_dims)
opt = Adam(lr=0.0002, beta_1=0.5)
gan = GAN(disc, gen, opt)

dataset_size = 60000
batch_size = 256
epochs = 2
batches = dataset_size // batch_size
iterations = batches*epochs
steps = 20
fps = 16

plots, labels = gan.train(data_gen, iterations, batch_size, verbose=1, freq_generation=steps, n_generations=25)

anim = Animation_MNIST(plots)
anim_video = anim.get_animation(fps=fps)
#anim.save(anim_video, fps, "/tmp",filename="mnist_first_epoch")
plt.show()
