from pygans.models import Discriminator, GAN, Generator
from pygans.animation import Animation_2D
from pygans.dataset import Dataset_Generator_2D

import matplotlib.pyplot as plt

from keras.models import Sequential
from keras.layers import Dense

def f(x):
  #return x**5 - 8*x**3 + 10*x + 6 
  #return 3*x**4 - 8*x**3 - 6*x**2 + 24*x - 2
  return x**2

def create_generator(latent_dims, outputs=2):
  model = Sequential()
  model.add(Dense(15, activation="relu", input_dim=latent_dims))
  model.add(Dense(8, activation="relu"))
  model.add(Dense(6, activation="relu"))
  model.add(Dense(outputs, activation="linear"))
  return model

def create_discriminator(inputs=2):
  model = Sequential()
  model.add(Dense(25, activation = "relu", input_dim=inputs, name="Input_Layer"))
  model.add(Dense(25, activation = "relu"))
  model.add(Dense(25, activation = "relu"))
  model.add(Dense(1, activation = "sigmoid", name="Output_Layer"))
  model.compile(loss="binary_crossentropy", optimizer="adam", metrics=["accuracy"])
  return model

def main():
  # Generation Parameters
  lower_limit = -5
  upper_limit = 5
  steps = 50
  fps = 20
  # Hyperparameters
  latent_dims = 5
  iterations = 2000
  batch_size = 128

  # Dataset Generation
  dataset_generator = Dataset_Generator_2D(f, lower_limit, upper_limit)

  # Create Models
  disc_model , gen_model = create_discriminator(), create_generator(latent_dims)
  discriminator = Discriminator(disc_model)
  generator = Generator(gen_model, latent_dims)
  gan = GAN(discriminator, generator)
  plots, labels = gan.train(dataset_generator, iterations, batch_size, freq_generation=steps)

  # Animation
  anim = Animation_2D(plots, lower_limit, upper_limit, labels)
  anim.draw_function(f)
  anim_video = anim.get_animation(fps=fps)
  #anim.save(anim_video, fps, "/tmp")
  plt.show()

main()